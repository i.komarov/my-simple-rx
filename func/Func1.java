package rx.func;

/**
 * Created by i_komarov on 16.11.16.
 */
public interface Func1<T, R> {

    R call(T object);
}
