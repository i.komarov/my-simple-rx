package rx.subscriber.interfaces;

/**
 * Created by i_komarov on 16.11.16.
 */
public interface ISubscriber2<T> {

    void onNext(T object);

    void onError(Throwable error);
}
