package rx.observable.hot.interfaces;

import rx.action.Action1;

/**
 * Created by i_komarov on 16.11.16.
 */
public interface ICollectionObservable<T> {

    public IObservable<T> forEach(Action1 action);
}
