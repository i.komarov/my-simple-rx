package rx.observable.hot.interfaces;

import rx.action.Action1;
import rx.func.Func1;
import rx.observable.hot.implemetation.Observable;
import rx.subscriber.interfaces.ISubscriber1;
import rx.subscriber.interfaces.ISubscriber2;
import rx.subscriber.interfaces.ISubscriber3;

import java.util.List;

/**
 * Created by i_komarov on 16.11.16.
 */
public interface IObservable<T> {

    public <R> IObservable<R> map(Func1<T, R> mapper);

    public IObservable<T> filter(Func1<T, Boolean> predicate);

    public IObservable<List<T>> toList();

    public T first();

    public <R> IObservable<R> concatMap(Class<R> clazz);

    public <I> IObservable<T> forEach(Action1<I> action);

    public void subscribe(ISubscriber1<T> subscriber);

    public void subscribe(ISubscriber2<T> subscriber);

    public void subscribe(ISubscriber3<T> subscriber);
}
