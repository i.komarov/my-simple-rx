package rx.observable.hot.implemetation;

import rx.action.Action1;
import rx.func.Func1;
import rx.observable.hot.interfaces.ICollectionObservable;
import rx.observable.hot.interfaces.IObservable;
import rx.subscriber.interfaces.ISubscriber1;
import rx.subscriber.interfaces.ISubscriber2;
import rx.subscriber.interfaces.ISubscriber3;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by i_komarov on 16.11.16.
 */
public class Observable<T> implements IObservable<T> {

    private List<IObservable<T>> children;
    private T object;

    protected Observable(T object) {
        this.object = object;
    }

    /** Constructor responsible for adding support of with-each like methods */
    protected Observable(Iterable<T> iterable) {
        children = new ArrayList<IObservable<T>>();

        Iterator<T> iterator = iterable.iterator();
        T first = null;

        if(iterator.hasNext()) {
            first = iterator.next();
        }

        if(!(first instanceof IObservable)) {
            children.add(Observable.just(first));
            while(iterator.hasNext()) {
                children.add(Observable.just(iterator.next()));
            }
        } else {
            children.add((IObservable<T>) first);
            while(iterator.hasNext()) {
                children.add((IObservable<T>) iterator.next());
            }
        }
    }

    /** Creates an instance of Observable with first item 'object' of S type */
    public static <S> IObservable<S> just(S object) {
        return new Observable<S>(object);
    }

    /** Converts an existing observable with object of type T to one
     * with object type R according to mapper */
    public <R> IObservable<R> map(Func1<T, R> mapper) {
        if(children == null) {
            return new Observable<R>(mapper.call(object));
        } else if(children.size() != 0) {
            List<IObservable<R>> newList = new ArrayList<>();

            for(IObservable<T> child : children) {
                newList.add(child.map(mapper));
            }

            return new Observable<R>((Iterable<R>) newList);
        }

        return new EmptyObservable<R>();
    }

    /** Filters observable's object by predicate. If predicate failed on object, an empty observable
     *  that skips all methods is passed as a result */
    public IObservable<T> filter(final Func1<T, Boolean> predicate) {
        //check if should use single-object filter
        if(children == null) {
            if (predicate.call(object)) {
                return this;
            }
        }
        //check if should use with-each-child fiter
        else if(children.size() != 0) {
            List<IObservable<T>> childrenToRemove = new ArrayList<>();
            for(IObservable<T> child : children) {
                if(child.filter(predicate) instanceof EmptyObservable) {
                    //children.remove(child);
                    childrenToRemove.add(child);
                }
            }
            for(IObservable<T> childToRemove : childrenToRemove) {
                children.remove(childToRemove);
            }

            return this;
        }
        //finally, if nothing fits returns an empty Observable
        return new EmptyObservable<T>();
    }

    public static <R> IObservable<R> from(Collection<R> collection) {
        //creates an Observable with child collection to use with-each-child methods
        return new Observable<R>(collection);
    }

    public static <R> IObservable<R> from(R... array) {
        if(array != null) {
            return new Observable<R>(Arrays.asList(array));
        }

        return new EmptyObservable<R>();
    }

    public <R> IObservable<R> concatMap(Class<R> input) {
        try {
            return Observable.from((R[]) object);
        } catch(ClassCastException e1) {
            try {
                return Observable.from((Collection<R>) object);
            } catch(ClassCastException e2) {
                return new EmptyObservable<R>();
            }
        }
    }

    public <I> IObservable<T> forEach(Action1<I> action) {
        if(children == null) {
            Object[] objects = (Object[]) object;
            for(Object obj : objects) {
                action.call((I) obj);
            }

            return this;
        } else if(children.size() != 0) {
            for(IObservable<T> child : children) {
                action.call((I) child.first());
            }

            return this;
        }

        return new EmptyObservable<T>();
    }

    private static <R> R convertObject(Object o, Class<R> clazz) {
        return clazz.cast(o);
    }

    public IObservable<List<T>> toList() {
        if(children != null) {
            List<T> list = new ArrayList<T>();
            for(IObservable<T> child : children) {
                list.add(child.first());
            }

            return Observable.just(list);
        }

        return new EmptyObservable<List<T>>();
    }

    public T first() {
        return this.object;
    }

    public <R> IObservable<R> from() {
        if(object instanceof Iterable) {
            Iterator iterator = ((Iterable) object).iterator();
            if(iterator.hasNext()) {
                return new Observable<R>((Iterable<R>) object);
            }
        }

        return new EmptyObservable<R>();
    }

    /** Subscribes an Observable to a subscriber with only onNext method implemented */
    public void subscribe(ISubscriber1<T> subscriber1) {
        if(children == null) {
            try {
                subscriber1.onNext(object);
            } catch (Exception e) {
                new OnErrorNotImplementedException().printStackTrace();
            }
        } else if(children.size() != 0) {
            for(IObservable<T> child : children) {
                child.subscribe(subscriber1);
            }
        }
    }

    /** Subscribes an Observable to a subscriber with only onNext and onError methods implemented */
    public void subscribe(ISubscriber2<T> subscriber2) {
        if(children == null) {
            try {
                subscriber2.onNext(object);
            } catch (Exception e) {
                subscriber2.onError(e);
            }
        } else if(children.size() != 0) {
            for(IObservable<T> child : children) {
                child.subscribe(subscriber2);
            }
        }
    }

    /** Subscribes an Observable to a subscriber with onNext, onError and onCompleted methods implemented */
    public void subscribe(ISubscriber3<T> subscriber3) {
        if(children == null) {
            try {
                subscriber3.onNext(object);
            } catch (Exception e) {
                subscriber3.onError(e);
            }
        } else if(children.size() != 0) {
            for(IObservable<T> child : children) {
                child.subscribe(subscriber3);
            }
        }
    }

    private static class EmptyObservable<T> implements IObservable<T> {

        EmptyObservable() {

        }

        /** Returns a new instance of EmptyObservable to fit the contract */
        public <R> IObservable<R> map(Func1<T, R> mapper) {
            return new EmptyObservable<R>();
        }

        /** Just returns the current instance as it already failed filter */
        public IObservable<T> filter(Func1<T, Boolean> predicate) {
            return this;
        }

        public <R> IObservable<R> concatMap(Class<R> clazz) {
            return new EmptyObservable<R>();
        }

        public <I> IObservable<T> forEach(Action1<I> action) {
            return this;
        }

        public T first() {
            return null;
        }

        public IObservable<List<T>> toList() {
            return new EmptyObservable<List<T>>();
        }

        /** Subscribes an Observable to a subscriber. As the filter failed, nothind would be passed to it */
        public void subscribe(ISubscriber1<T> subscriber1) {

        }

        /** Subscribes an Observable to a subscriber. As the filter failed, nothind would be passed to it */
        public void subscribe(ISubscriber2<T> subscriber2) {

        }

        /** Subscribes an Observable to a subscriber. As the filter failed, nothind would be passed to it */
        public void subscribe(ISubscriber3<T> subscriber3) {

        }
    }

    private static class ThrowableObservable<T, I extends Throwable> extends EmptyObservable<T> {

        protected Throwable throwable;

        ThrowableObservable(Throwable throwable) {
            super();
            this.throwable = throwable;
        }

        public <R> IObservable<R> map(Func1<T, R> mapper) {
            return new ThrowableObservable<R, I>(throwable);
        }

        public IObservable<T> filter(Func1<T, Boolean> predicate) {
            return this;
        }

        /**  */
        public void subscribe(ISubscriber1<T> subscriber1) {
            try {
                throw new OnErrorNotImplementedException();
            } catch (OnErrorNotImplementedException e) {
                e.printStackTrace();
            }
        }

        public void subscribe(ISubscriber2<T> subscriber2) {
            if(throwable != null) {
                subscriber2.onError(throwable);
            }
        }

        public void subscribe(ISubscriber3<T> subscriber3) {
            if(throwable != null) {
                subscriber3.onError(throwable);
            }
        }
    }

    public static class OnErrorNotImplementedException extends Exception {

        private static final String MESSAGE = "Error occured during completing the chain. To handle the exception implement the onError() method to the subscriber interface";

        public OnErrorNotImplementedException() {
            super(MESSAGE);
        }
    }
}
